(function(angular) {
  var AppController = function($scope, Person) {
    Person.query(function(response) {
      $scope.persons = response ? response : [];
    });

    $scope.addPerson = function(personName, personAge) {
      new Person({
        name: personName,
        age: personAge
      }).$save(function(person) {
        $scope.persons.push(person);
      });
      $scope.personName = "";
      $scope.personAge = "";
    };

    $scope.updatePerson = function(person) {
      person.$update();
    };

    $scope.deletePerson = function(person) {
      person.$remove(function() {
        $scope.persons.splice($scope.persons.indexOf(person), 1);
      });
    };
  };

  AppController.$inject = ['$scope', 'Person'];
  angular.module("myApp.controllers").controller("AppController", AppController);
}(angular));