package hello.angular.repo;

import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Konrad on 04.09.2016:10:14
 */

@Repository
public class PersonRepo {
    private final Map<Integer, Person> repo = new ConcurrentHashMap<>();
    private final AtomicInteger sequence = new AtomicInteger(0);

    public List<Person> findAll(){
        return new ArrayList<>(repo.values());
    }

    public boolean delete(Person person) {
        return delete(person.getId());
    }

    public boolean delete(Integer id) {
        if (!repo.containsKey(id)) {
            return false;
        } else {
            repo.remove(id);
            return true;
        }
    }

    public Person put(Person person) {
        if (person.getId() == null) {
            int id = genNextId();
            repo.put(id, person.setId(id));
        } else {
            updateSequence(person.getId());
            repo.put(person.getId(), person);
        }
        return person;
    }

    private void updateSequence(Integer id) {
        int seq = sequence.get();
        if (seq < id) {
            if (!sequence.compareAndSet(seq, id)) {
                updateSequence(id);
            }
        }
    }

    private int genNextId() {
        return sequence.getAndIncrement();
    }

}
