package hello.angular.repo;

/**
 * Created by Konrad on 04.09.2016:10:11
 */
public class Person {
    private Integer id;
    private String name;
    private String info = "No personal record.";
    private Integer age;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getId() {
        return id;
    }

    public Person setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
