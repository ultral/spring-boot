package hello.angular.controller;

import hello.angular.info.InfoSearcher;
import hello.angular.repo.Person;
import hello.angular.repo.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Konrad on 04.09.2016:10:29
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/persons")
public class PersonController {
    @Autowired
    private PersonRepo repo;

    @Autowired
    private InfoSearcher info;

    @SuppressWarnings("unused")
    @RequestMapping(method = RequestMethod.GET)
    public List findItems() {
        return repo.findAll();
    }

    @SuppressWarnings("unused")
    @RequestMapping(method = RequestMethod.POST)
    public Person addPerson(@RequestBody Person person) {
        person.setId(null);
        return putPersonAndStartSearch(person);
    }

    @SuppressWarnings("unused")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Person updatePerson(@RequestBody Person updatedPersson, @PathVariable Integer id) {
        updatedPersson.setId(id);
        return putPersonAndStartSearch(updatedPersson);
    }

    private Person putPersonAndStartSearch(@RequestBody Person updatedPersson) {
        Person person = repo.put(updatedPersson);
        info.searchForData(person);
        return person;
    }

    @SuppressWarnings("unused")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id) {
        repo.delete(id);
    }
}
