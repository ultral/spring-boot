package hello.benchmark;

import hello.benchmark.collection.CollectionBenchmarkInterfeace;
import javaslang.collection.List;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Konrad on 29.08.2016:21:19
 */
class OutputFormatter {

    private final static String br = "<br/>";
    private final static String separator = "++++++++++";
    private static final NumberFormat FORMATTER = new DecimalFormat("#0.000000");
    private static final int SECONDS = 1_000_000_000;
    private static final int MILIS = 1_000_000;

    private OutputFormatter() {
    }

    static OutputFormatter crete() {
        return new OutputFormatter();
    }

    String createReadeableOutput(List<Long> results, CollectionBenchmarkInterfeace bench) {
        String o = "";
        o += "Test: " + bench.getName() + br;
        o += median(results) + br;
        o += average(results) + br;
        o += shortest(results) + br;
        o += longest(results) + br;
        o += br;
        o += "Executed code:" + br;
        o += "<pre>" + bench.getDetailedDescription() + "</pre>";


        return o;
    }

    private String median(List<Long> results) {
        List<Long> sorted = results.sorted();
        int middle = sorted.length() / 2;
        double median;
        if (sorted.length() % 2 == 1) {
            median = sorted.get(middle);
        } else {
            median = (sorted.get(middle - 1) + sorted.get(middle)) / 2.0;
        }

        return formatString(median, "Median");
    }

    private String average(List<Long> results) {
        return formatString(
                results.isEmpty() ? 0 : (results.sum().doubleValue() / results.size()), "Average");
    }

    private String shortest(List<Long> results) {
        return formatString(results.min().getOrElse(0L), "Shortest");
    }

    private String longest(List<Long> results) {
        return formatString(results.max().getOrElse(0L), "Longest");
    }

    private String formatString(double average, String name) {
        return name + " execution took: "
                + FORMATTER.format(average / SECONDS)
                + " seconds ("
                + FORMATTER.format(average / MILIS) + " milis)";
    }

    String formatOutput(String result1, String result2) {
        return result1 + br + br + separator + br + br + result2;
    }

    String enviroment() {
        String o = "";
        o += "Java version: '" + System.getProperty("java.version") + "'" + br;
        o += "Avaliable logical cores: '" + Runtime.getRuntime().availableProcessors() + "'" + br;
        long maxBytes = Runtime.getRuntime().maxMemory();
        o += "Availabe memory: '" + (double) maxBytes / 1_000_000 + "' MB (" + maxBytes + " bytes)" + br;
        o += "Operating System: '" + System.getProperty("os.name") + "'";
        return o;
    }

    String testSpecification(int testsNumber, int testSize) {
        String o = "";
        o += "Test collection size: '" + testSize + "'" + br;
        o += "Number of test: '" + testsNumber + "'" + br;
        o += br;
        o += "You can change benchmark params:" + br;
        o += "/benchmark?testSize=" + testSize + "&testNumber=" + testsNumber;
        return o;
    }
}
