package hello.benchmark.collection;

/**
 * Created by Konrad on 28.08.2016:21:31
 */
public interface CollectionBenchmarkInterfeace {
    int filterValue = 4;

    int run();

    String getDetailedDescription();

    String getName();
}
