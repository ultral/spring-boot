package hello.benchmark.collection;

import java.util.Collection;

/**
 * Created by Konrad on 28.08.2016:21:34
 */
public class CollectionBenchmarkStream implements CollectionBenchmarkInterfeace {
    Collection<Integer> coll;

    public static CollectionBenchmarkStream init(Collection<Integer> collection) {
        return new CollectionBenchmarkStream(){{this.coll = collection;}};
    }

    @Override
    public int run() {
        return coll.stream().filter(i -> i > filterValue).mapToInt(i -> i).sum();
    }

    @Override
    public String getDetailedDescription() {
        return "        return coll.stream().filter(i -> i > filterValue).mapToInt(i -> i).sum();";
    }

    @Override
    public String getName() {
        return "Java Stream Collection Iteration";
    }
}
