package hello.benchmark.collection;

import javaslang.collection.List;

import java.util.Collection;

/**
 * Created by Konrad on 28.08.2016:21:34
 */
public class CollectionBenchmarkJavaslang implements CollectionBenchmarkInterfeace {
    List<Integer> javaslangList;

    public static CollectionBenchmarkJavaslang init(Collection<Integer> collection) {
        return new CollectionBenchmarkJavaslang(){{this.javaslangList = List.ofAll(collection);}};
    }

    @Override
    public int run() {
        return javaslangList.filter(i -> i > filterValue).sum().intValue();
    }

    @Override
    public String getDetailedDescription() {
        return "        return javaslangList.filter(i -> i > filterValue).sum().intValue();";
    }

    @Override
    public String getName() {
        return "JavasLang List Iteration";
    }

}
