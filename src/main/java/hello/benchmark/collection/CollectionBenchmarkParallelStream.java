package hello.benchmark.collection;

import java.util.Collection;

/**
 * Created by Konrad on 28.08.2016:21:34
 */
public class CollectionBenchmarkParallelStream implements CollectionBenchmarkInterfeace {
    Collection<Integer> coll;

    public static CollectionBenchmarkParallelStream init(Collection<Integer> collection) {
        return new CollectionBenchmarkParallelStream(){{this.coll = collection;}};
    }

    @Override
    public int run() {
        return coll.parallelStream().filter(i -> i > filterValue).mapToInt(i -> i).sum();
    }

    @Override
    public String getDetailedDescription() {
        return "        return coll.parallelStream().filter(i -> i > filterValue).mapToInt(i -> i).sum();";
    }

    @Override
    public String getName() {
        return "Java Parallel Stream Collection Iteration";
    }
}
