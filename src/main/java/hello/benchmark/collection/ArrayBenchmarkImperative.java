package hello.benchmark.collection;

import java.util.Collection;

/**
 * Created by Konrad on 28.08.2016:21:34
 */
public class ArrayBenchmarkImperative implements CollectionBenchmarkInterfeace {
    int[] array;

    public static ArrayBenchmarkImperative init(Collection<Integer> collection) {
        Integer[] arr = new Integer[collection.size()];
        collection.toArray(arr);
        int[] arr2 = new int[collection.size()];
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = arr[i];
        }
        return new ArrayBenchmarkImperative(){{this.array = arr2;}};
    }

    @Override
    public int run() {
        int sum = 0;
        for (int value : array)
            if (value > filterValue)
                sum += value;
        return sum;
    }

    @Override
    public String getDetailedDescription() {
        return  "        int sum = 0;\n" +
                "        for (int value : array) \n" +
                "            if (value > filterValue) \n" +
                "                sum += value;\n" +
                "        return sum;";
    }

    @Override
    public String getName() {
        return "Java Imperative Array Iteration";
    }
}
