package hello.benchmark.collection;

import java.util.Collection;

/**
 * Created by Konrad on 28.08.2016:21:34
 */
public class CollectionBenchmarkImperative implements CollectionBenchmarkInterfeace {
    Collection<Integer> coll;

    public static CollectionBenchmarkImperative init(Collection<Integer> collection) {
        return new CollectionBenchmarkImperative(){{this.coll = collection;}};
    }

    @Override
    public int run() {
        int sum = 0;
        for (int value : coll)
            if (value > filterValue)
                sum += value;
        return sum;
    }

    @Override
    public String getDetailedDescription() {
        return  "        int sum = 0;\n" +
                "        for (int value : coll) \n" +
                "            if (value > filterValue) \n" +
                "                sum += value;\n" +
                "        return sum;";
    }

    @Override
    public String getName() {
        return "Java Imperative Collection Iteration";
    }
}
