package hello.benchmark;

import hello.benchmark.collection.*;
import javaslang.collection.List;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;


/**
 * Created by Konrad on 27.08.2016:22:52
 */
@Component
public class BenchamarkEngine {

    private final OutputFormatter of = OutputFormatter.crete();

    //private BenchamarkEngine(){}

    private static final int DEFAULT_TEST_SIZE = 1000;
    private static final int DEFAULT_TESTS_NUMBER = 1000;

    private int testsNumber = DEFAULT_TESTS_NUMBER;
    private int testSize = DEFAULT_TEST_SIZE;

    public static BenchamarkEngine create(){
        return new BenchamarkEngine();
    }

    public BenchamarkEngine withTestSize(Integer testSize){
        this.testSize = testSize == null || testSize < 1 ? DEFAULT_TEST_SIZE : testSize;
        return this;
    }

    public BenchamarkEngine withTestsNumber(Integer testsNumber){
        this.testsNumber = testsNumber == null || testsNumber < 1 ? DEFAULT_TESTS_NUMBER : testsNumber;
        return this;
    }

    private String runBenchmark(CollectionBenchmarkInterfeace bench, int testsNumber) {
        runNTimes(testsNumber/100, bench);//warmup
        return of.createReadeableOutput(runNTimes(testsNumber, bench), bench);
    }

    private List<Long> runNTimes(int n, CollectionBenchmarkInterfeace benchmark) {
        List<Long> results = List.empty();
        for (int i = 0; i < n; i++) {
            results = results.append(runOnce(benchmark));
        }
        return results;
    }

    private long runOnce(CollectionBenchmarkInterfeace benchmark) {
        long startTime = System.nanoTime();
        benchmark.run();
        long stopTime = System.nanoTime();
        return stopTime - startTime;
    }

    public String runBenchmarks(){
        Collection<Integer> testCollection = createTestCollection(testSize);

        List<CollectionBenchmarkInterfeace> benchmarkList = List.of(
                CollectionBenchmarkImperative.init(testCollection),
                CollectionBenchmarkStream.init(testCollection),
                CollectionBenchmarkParallelStream.init(testCollection),
                CollectionBenchmarkJavaslang.init(testCollection),
                ArrayBenchmarkImperative.init(testCollection)
        );

        List<String> benchmarkResults = List.of(of.enviroment(), of.testSpecification(testsNumber, testSize))
                .appendAll(benchmarkList.map(benchmark -> runBenchmark(benchmark, testsNumber)).toList());

        return benchmarkResults.reduce(of::formatOutput);
    }

    private Collection<Integer> createTestCollection(int testSize) {
        Random r = new Random(System.currentTimeMillis());
        java.util.List<Integer> testCollection = new ArrayList<>(testSize);
        for (int i = 0; i < testSize; i++) {
            testCollection.add(r.nextInt());
        }

        return testCollection;
    }

}
