package hello;

import hello.benchmark.BenchamarkEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Konrad on 27.08.2016:22:34
 */
@Controller
@ComponentScan
@EnableAutoConfiguration
public class SampleController {

    private BenchamarkEngine be;

    @Autowired
    public SampleController(final BenchamarkEngine be) {
        this.be = be;
    }

    @SuppressWarnings("unused")
    @RequestMapping(path = "/ha", method = RequestMethod.GET)
    String ha() {
        return "redirect:" + "http://www.pown.it/1681";
    }

    @SuppressWarnings("unused")
    @RequestMapping(path = "/angular", method = RequestMethod.GET)
    String angular() {
        return "forward:" + "/";
    }

    @SuppressWarnings("unused")
    @RequestMapping(path = "/you", method = RequestMethod.GET)
    String trololo() {
        return "redirect:" + "https://youtu.be/sCNrK-n68CM";
    }

    @SuppressWarnings("unused")
    @ResponseBody
    @RequestMapping(path = "/benchmark", method = RequestMethod.GET)
    String benchmarkSizeNumber(@RequestParam(name = "testSize", required = false) Integer testSize,
                               @RequestParam(name = "testNumber", required = false) Integer testsNumber) {
        return be.withTestSize(testSize)
                .withTestsNumber(testsNumber)
                .runBenchmarks();
    }

/*
    @RequestMapping("*")
    @ResponseBody
    public String fallbackMethod(){
        return "seriously?";
    }
*/

    public static void main(String[] args) {
        SpringApplication.run(SampleController.class, args);
    }
}
